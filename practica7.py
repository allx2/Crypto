import multiprocessing as pSearch
import re


def cifrado(k,m):
    c = ''
    for i in m:
        value=ord(i) + k
        if value < 127:
           c+=chr(value)
        else:
           c+=chr(value-94)
    return c


def descifrado(k,m):
    c = ''
    for i in m:
        value=ord(i) - k
        if value > 32:
            c+=chr(value)
        else:
            c+=chr(value+94)
    return c



def Search_word(text,file,Search):
    f = open(file,"r")
    count=0
    for i in f.read():
        a = len(re.findall(i,text))
        count += a
    Search.put(count)

def _main_():
    Search = pSearch.Queue()
    texto = input("Cadena a buscar")
    f = open("es.txt","r")
    g = open("en.txt","r")
    processes = [Search.Process(target=Search_word, args=(texto,"es.txt", Search)),Search.Process(target=Search_word, args=(texto,"en.txt", Search)) ]

    for p in processes:
        p.start()
    for p in processes:
        p.join()

    results = [Search.get() for p in processes]