#! /usr/bin/python3

###script creado por msconker para la clase de criptografia

def cifrado(k,m):
    c = ''
    for i in m:
        value=ord(i) + k
        if value < 127:
           c+=chr(value)
        else:
           c+=chr(value-94)
    return c


def descifrado(k,m):
    c = ''
    for i in m:
        value=ord(i) - k
        if value > 32:
            c+=chr(value)
        else:
            c+=chr(value+94)
    return c

def main(key=0,message='',option=1):
    key = int(input("Clave:"))
    message = input("mensaje:")
    option = int(input("1)Cifrar\n2)Descrifrar\n>"))

    if option == 1:
        message= cifrado(key,message)
    elif option == 2:
        message= descifrado(key,message)
    else:
        print("opcion no valida")
        main()
    print("mensaje cifrado:",message)

if __name__ == "__main__":
    main()