#! /usr/bin/python3

###Atque de fuerza bruta para cifrado cesar, usando diccionario de frecuencias de la RAE
### desarrollado por msconker

import cesar


message = input("message:")

###diccionario de frecuencias de palabras en español###
fraq_dict = open("10000_formas.TXT", "r")


def wordcheck(w):
    if w in fraq_dict.read():
        return True
    else:
        return False


def main():
    for i in range(26):
        valid = 0
        for j in cesar.cifrado(message, i):
            if wordcheck(j):
                valid += 1
        if valid == len(cesar.cifrado(message, i).split()):
                print ("mensaje:", message)
                print ("key:", i)
                break


if __name__ == "__main__":
    main()